package models;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServidorMessage {

	public static void main(String[] args) {
		// Abrir RMIRegister en el puerto por defecto 1099
		try {
			final Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
			System.out.println("Registro obtenido.");
			
			String nameService = "Message";
			
			// Crear stub (conexi�n entre el objeto remoto y el registro)
			MessageInterface server = new MessageInterfaceImpl();
			MessageInterface stub = (MessageInterface) UnicastRemoteObject.exportObject(server, 0);
			System.out.println("Stub creado.");
			
			// Registrar stub en el RMIRegistry
			registry.rebind(nameService, stub);
			System.out.println("Servicio a�adido.");
			
			// Esperando tecla para finalizar.
			System.out.println("Pulse una tecla cualquiera para finalizar...");
			System.in.read();
			System.out.println("Cerrando servidor...");
			// Quitar stub del RMIRegistry
			registry.unbind(nameService);
			UnicastRemoteObject.unexportObject(server, true);
			System.out.println("Servidor finalizado.");
			
		} catch (RemoteException e) {
			System.err.println("Error de comunicaci�n: " + e.toString());
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Excepci�n en Servidor Message: " + e.toString());
			System.exit(1);
		} catch (NotBoundException e) {
			System.err.println("Excepci�n en Servidor Message: " + e.toString());
			System.exit(1);
		}
	}
	
}
