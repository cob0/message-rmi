package models;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MessageInterface extends Remote {
	
	public void sendMessage(String from, String msg) throws RemoteException;

}
