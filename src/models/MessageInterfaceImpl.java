package models;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Date;

public class MessageInterfaceImpl implements Serializable, MessageInterface {

	private static final long serialVersionUID = -645252914357005009L;

	@Override
	public void sendMessage(String from, String msg) throws RemoteException {
		if((from != null && !from.isEmpty())
				&& (msg != null && !msg.isEmpty()))
			System.out.println(new Date(System.currentTimeMillis()) + " " + from + ": " + msg);
	}

}
