package models;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Cliente {
	
	private String remitente;
	
	public Cliente(String remitente) {
		this.remitente = remitente;
	}
	
	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		String nameService = "Message";
		String message = "";
		Cliente cliente;
		
		// Pedimos el remitente
		System.out.print("Remitente: ");
		String from = teclado.nextLine();
		cliente = new Cliente(from != null && !from.isEmpty() ? from : "default");
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		
		try {
			// Usar el puerto por defecto
			Registry registry = LocateRegistry.getRegistry("localhost");
			System.out.println("Registro obtenido.");
			while(!message.equalsIgnoreCase("salir")) {
				MessageInterface messageInterface = (MessageInterface) registry.lookup(nameService);
				System.out.print("Mensaje: ");
				messageInterface.sendMessage(cliente.getRemitente(), message = teclado.nextLine());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}

}
